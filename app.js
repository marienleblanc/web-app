var http = require("http");
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: true });

//redis configuration
var redis = require('redis');
var client = redis.createClient();
var acc = new Object();
var hom = new Object();
var ring = new Object();

//check node/redis connection
client.on('connect', function() {
    console.log('connected');
});

//Use db1
client.select(1);

// Configure view engine to render EJS templates.
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');


// Running Server Details.
var server = app.listen(80/*, "10.0.0.4"*/, function () {
  var host = server.address().address
  var port = server.address().port
  console.log("Example app listening at %s:%s Port", host, port)
});
 
app.get('/', function(req, res) {
  res.render('index');
});

app.get('/contact', function(req, res) {
  res.render('contact');
});

app.get('/form-my-account', function (req, res) {
  res.render('form-my-account');
});
 
app.post('/my-account', urlencodedParser, function (req, res){
  acc.name = req.body.name;
  acc.email =  req.body.email; 
  acc.address =  req.body.address;
  acc.city =  req.body.city;
  var jsonString = JSON.stringify(acc);
  console.log(jsonString);
  client.incr('id', function(err, id){client.set('account'+id, jsonString);});

  res.render("my-account", {name: req.body.name}); 
 });

app.get('/form-my-home', function (req, res) {
  res.render('form-my-home');
});

app.post('/my-home', urlencodedParser, function (req, res) {
  hom.typeHome = req.body.typeHome;
  hom.floors = req.body.floors;
  hom.rooms = req.body.rooms;
  hom.rings = req.body.rings;
  var jsonString = JSON.stringify(hom);
  console.log(jsonString);
  client.incr('home', function(err, id){
    client.set('home'+id, jsonString);
  });

  res.render("my-home");
});

app.get('/form-my-rings', function (req, res) {
  var currId = 0;
  client.get('id', function(error, value){
    currId = value;
    var home = "home"+currId;
    client.get(home, function(error, value){
      var jsonString = JSON.parse(value);
      var typeHome = jsonString['typeHome'];
      var floors = jsonString['floors'];
      var rooms = jsonString['rooms'];
      var rings = jsonString['rings'];
      res.render('form-my-rings', {rings, floors});
    });

  });
});

app.post('/my-rings', urlencodedParser, function (req, res) {
  var currId = 0;
  client.get('id', function(error, value){
    currId = value;
    var home = "home"+currId;
    client.get(home, function(error, value){
      var jsonString = JSON.parse(value);
      var rings = jsonString['rings'];
      
      ring.ringz = req.body;
      var jsonString2 = JSON.stringify(ring);
      console.log(jsonString2);
      client.incr('rings', function(err, id){
        client.set('rings'+id, jsonString2);
      });
    
    res.render('my-rings');
    
    });
  });
});